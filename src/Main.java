/*hw3)Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран. Напоминаем, что первый
и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих.*/
public class Main {
    public static void main (String[] args) {
        int[] mas = new int[20];
        mas[0] = 0;
        mas[1] = 1;
        for (int i = 2; i < 20; i++) {
            mas[i] = mas[i - 1] + mas[i - 2];
        }
        for (int elem : mas) {
            System.out.println(elem);
        }
    }
}